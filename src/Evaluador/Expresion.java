/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluador;

import ufps.util.colecciones_seed.*;

/**
 *
 * @author Duban Silva - Natalia Garcia
 */
public class Expresion {

    private ListaCD<String> expresiones = new ListaCD();

    public Expresion() {
    }

    public Expresion(String cadena) {

        String v[] = cadena.split(",");
        for (String dato : v) {
            this.expresiones.insertarAlFinal(dato);
        }
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    //Valida la expresión para poder mostrar la respuesta
    public String validarExpresion() {

        Object x[] = this.expresiones.aVector();
        String cadena = "";
        String msg = "";
        cadena += x[0];
        for (int i = 1; i < x.length; i++) {
            cadena += "," + x[i];
        }
        if (validarParentesis(cadena)) {
            msg += "Resultado de Posfijo: " + getPosfijo() + "\n";
            msg += "Resultado de Prefijo: " + getPrefijo(invertirCadena(getPosfijo())) + "\n";
            msg += "Resultado de la Operación: " + getEvaluarPosfijo() + "\n";
        } else {
            msg += "Error de la expresión: " + cadena + "\n";
        }
        return msg;
    }

    //Invierte la cadena para poder hacer la operación del prefijo
    private String invertirCadena(String cadena) {
        String[] x = cadena.split(",");
        String msg = "";
        msg += x[x.length - 1];

        for (int i = x.length - 2; i >= 0; i--) {
            msg += "," + x[i];
        }
        return msg;
    }

    //Valida los parentesis en la cadena 
    private boolean validarParentesis(String cadena) {
        Pila<String> p = new Pila<>();
        String[] x = cadena.split(",");
        for (String pa : x) {
            if (pa.equals('(')) {
                p.apilar(pa);
            } else if (p.equals(')')) {
                if (!p.esVacia()) {
                    p.desapilar();
                } else {
                    return false;
                }
            }
        }
        return p.esVacia();
    }

    //Valida los operadores de la cadena
    private boolean esOperador(String o) {
        return (o.equals("+") || o.equals("-") || o.equals("*") || o.equals("/") || o.equals("^"));
    }

    //Convierte la cadena de infijo a posfijo
    public String getPosfijo() {
        Pila<String> p = new Pila<>();
        ListaCD<String> posfijo = new ListaCD<>();

        for (String d : this.expresiones) {
            switch (d) {
                case "+":
                case "-":
                case "*":
                case "/":
                    if (p.esVacia() || p.getTope().equals("(")) {
                        p.apilar(d);
                    } else {
                        while (!p.esVacia()) {
                            if (getPrioridadExpresion(d) > getPilaPrioridad(p.getTope())) {
                                p.apilar(d);
                                break;
                            } else {
                                posfijo.insertarAlFinal(p.desapilar());
                                if (p.esVacia()) {
                                    p.apilar(d);
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case "(":
                    p.apilar(d);
                    break;
                case ")":
                    while (!p.getTope().equals("(")) {
                        posfijo.insertarAlFinal(p.desapilar());
                    }
                    p.desapilar();
                    break;
                default:
                    posfijo.insertarAlFinal(d);
                    break;
            }
        }
        while (!p.esVacia()) {
            posfijo.insertarAlFinal(p.desapilar());
        }
        return getPostfijoAndPrefijo(posfijo);
    }
    //Convierte la cadena de infijo a prefijo

    public String getPrefijo(String cadena) {
        String[] x = cadena.split(",");
        Pila<String> p = new Pila<>();
        ListaCD<String> prefijo = new ListaCD<>();

        for (String d : x) {
            switch (d) {
                case "+":
                case "-":
                case "*":
                case "/":
                    if (p.esVacia() || p.getTope().equals("(")) {
                        p.apilar(d);
                    } else {
                        while (!p.esVacia()) {
                            if (getPrioridadExpresion(d) > getPilaPrioridad(p.getTope())) {
                                p.apilar(d);
                                break;
                            } else {
                                prefijo.insertarAlInicio(p.desapilar());
                                if (p.esVacia()) {
                                    p.apilar(d);
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case ")":
                    p.apilar(d);
                    break;
                case "(":
                    while (!p.getTope().equals(")")) {
                        prefijo.insertarAlInicio(p.desapilar());
                    }
                    p.desapilar();
                    break;
                default:
                    prefijo.insertarAlInicio(d);
                    break;
            }
        }
        while (!p.esVacia()) {
            prefijo.insertarAlInicio(p.desapilar());
        }
        return getPostfijoAndPrefijo(prefijo);
    }

    private String getPostfijoAndPrefijo(ListaCD<String> cad) {
        String p = "";
        for (String d : cad) {
            p += d + ",";
        }
        return p;
    }

    //Evalua el posfijo, hace las operaciones para saber el resultado
    public float getEvaluarPosfijo() {
        String[] postfijo = this.getPosfijo().split(",");
        Pila<Float> p = new Pila();
        float operando1 = 0;
        float operando2 = 0;
        float resultado = 0;

        for (String postfijo1 : postfijo) {
            if (!esOperador(postfijo1)) {
                p.apilar(Float.parseFloat(postfijo1));
            } else {
                operando2 = p.desapilar();
                operando1 = p.desapilar();
                switch (postfijo1) {
                    case "-": {
                        resultado = operando1 - operando2;
                        p.apilar(resultado);
                        break;
                    }
                    case "+": {
                        resultado = operando1 + operando2;
                        p.apilar(resultado);
                        break;
                    }
                    case "*": {
                        resultado = operando1 * operando2;
                        p.apilar(resultado);
                        break;
                    }
                    case "/": {
                        resultado = operando1 / operando2;
                        p.apilar(resultado);
                        break;
                    }
                }
                p.apilar(resultado);
            }
        }
        return p.desapilar();
    }

    //Prioridades de la expresión para poder hacer la operación
    private int getPrioridadExpresion(String d) {
        int prioridadExpresion = 6;

        if (d.equals("(") || d.equals(")")) {
            prioridadExpresion = 5;
            return prioridadExpresion;
        }
        if (d.equals("*") || d.equals("/")) {
            prioridadExpresion = 4;
            return prioridadExpresion;
        }
        if (d.equals("+") || d.equals("-")) {
            prioridadExpresion = 3;
            return prioridadExpresion;
        }
        return prioridadExpresion;
    }

    //Prioridades de la pila para poder hacer la operacion
    private int getPilaPrioridad(String d) {
        int prioridadPila = 5;

        if (d.equals("*") || d.equals("/")) {
            prioridadPila = 4;
            return prioridadPila;
        }
        if (d.equals("+") || d.equals("-")) {
            prioridadPila = 3;
            return prioridadPila;
        }
        if (d.equals("(") || d.equals(")")) {
            prioridadPila = 0;
            return prioridadPila;
        }
        return prioridadPila;
    }

    public String toString() {
        String msg = "";
        for (String dato : this.expresiones) {
            msg += dato + "<->";
        }
        return msg;
    }
}
